<?php

namespace Prilichno\Censors\Contracts;

interface CensorInterface
{
    /**
     * @param  string  $text
     * @return bool
     */
    public function isAllowed(string $text): bool;

    /**
     * @param  string  $text
     * @return string
     */
    public function getFiltered(string $text): string;

}