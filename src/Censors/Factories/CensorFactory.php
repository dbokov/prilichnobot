<?php

namespace Prilichno\Censors\Factories;

use Prilichno\Censors\Contracts\CensorInterface;

class CensorFactory
{
    /**
     * @return CensorInterface
     * @throws \Exception
     */
    public static function get(): CensorInterface
    {
        $censorClassName = ($_ENV['CENSOR'] ?? 'Default').'Censor';
        $censorClass = "Prilichno\\Censors\\" .$censorClassName;
        if (!class_exists($censorClass)) {
            throw new \Exception('No censor class found');
        }
        return new $censorClass;
    }
}