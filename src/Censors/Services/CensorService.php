<?php

namespace Prilichno\Censors\Services;

use Prilichno\Censors\Contracts\CensorInterface;
use Prilichno\Censors\Factories\CensorFactory;

class CensorService
{
    /**
     * @var CensorInterface
     */
    private CensorInterface $censor;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->censor = CensorFactory::get();
    }

    /**
     * @param  string  $text
     * @return bool
     */
    public function isAllowed(string $text): bool
    {
        return $this->censor->isAllowed($text);
    }

    /**
     * @param  string  $text
     * @return string
     */
    public function getFiltered(string $text): string
    {
        return $this->censor->getFiltered($text);
    }
}