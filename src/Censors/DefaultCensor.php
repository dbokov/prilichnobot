<?php

namespace Prilichno\Censors;

use Prilichno\Censors\Contracts\CensorInterface;

class DefaultCensor implements CensorInterface
{

    private static string $LT_P = 'пПnPp';
    private static string $LT_I = 'иИiI1u';
    private static string $LT_E = 'еЕeE';
    private static string $LT_D = 'дДdD';
    private static string $LT_Z = 'зЗ3zZ3';
    private static string $LT_M = 'мМmM';
    private static string $LT_U = 'уУyYuU';
    private static string $LT_O = 'оОoO0';
    private static string $LT_L = 'лЛlL';
    private static string $LT_S = 'сСcCsS';
    private static string $LT_A = 'аАaA';
    private static string $LT_N = 'нНhH';
    private static string $LT_G = 'гГgG';
    private static string $LT_CH = 'чЧ4';
    private static string $LT_K = 'кКkK';
    private static string $LT_C = 'цЦcC';
    private static string $LT_R = 'рРpPrR';
    private static string $LT_H = 'хХxXhH';
    private static string $LT_YI = 'йЙy';
    private static string $LT_YA = 'яЯ';
    private static string $LT_YO = 'ёЁ';
    private static string $LT_YU = 'юЮ';
    private static string $LT_B = 'бБ6bB';
    private static string $LT_T = 'тТtT';
    private static string $LT_HS = 'ъЪ';
    private static string $LT_SS = 'ьЬ';
    private static string $LT_Y = 'ыЫ';
    private static string $LT_V = 'вВvWw';

    public static array $exceptions = [
        'команд',
        'рубл',
        'премь',
        'оскорб',
        'краснояр',
        'бояр',
        'ноябр',
        'карьер',
        'мандат',
        'употр',
        'плох',
        'интер',
        'веер',
        'фаер',
        'феер',
        'hyundai',
        'тату',
        'браконь',
        'roup',
        'сараф',
        'держ',
        'слаб',
        'ридер',
        'истреб',
        'потреб',
        'коридор',
        'sound',
        'дерг',
        'подоб',
        'коррид',
        'дубл',
        'курьер',
        'экст',
        'try',
        'enter',
        'oun',
        'aube',
        'ibarg',
        '16',
        'kres',
        'глуб',
        'ebay',
        'eeb',
        'shuy',
        'ансам',
        'cayenne',
        'ain',
        'oin',
        'тряс',
        'ubu',
        'uen',
        'uip',
        'oup',
        'кораб',
        'боеп',
        'деепр',
        'хульс',
        'een',
        'ee6',
        'ein',
        'сугуб',
        'карб',
        'гроб',
        'лить',
        'рсук',
        'влюб',
        'хулио',
        'ляп',
        'граб',
        'ибог',
        'вело',
        'ебэ',
        'перв',
        'eep',
        'ying',
        'laun',
        'чаепитие'
    ];

    /**
     * @param  string  $text
     * @return string
     */
    public function getFiltered(string $text): string
    {
        return $this->filterText($text);
    }

    /**
     * @param  string  $text
     * @return bool
     */
    public function isAllowed(string $text): bool
    {
        $original = $text;
        $text = $this->filterText($text);
        return $original === $text;
    }

    /**
     * @param  string  $text
     * @return string
     */
    public function filterText(string $text): string
    {
        $utf8 = 'UTF-8';

        preg_match_all(
            '/
\b\d*(
	\w*['.self::$LT_P.']['.self::$LT_I.self::$LT_E.']['.self::$LT_Z.']['.self::$LT_D.']\w* # пизда
|
	(?:[^'.self::$LT_I.self::$LT_U.'\s]+|'.self::$LT_N.self::$LT_I.')?(?<!стра)['.self::$LT_H.']['.self::$LT_U.']['.self::$LT_YI.self::$LT_E.self::$LT_YA.self::$LT_YO.self::$LT_I.self::$LT_L.self::$LT_YU.'](?!иг)\w* # хуй; не пускает "подстрахуй", "хулиган"
|
	\w*['.self::$LT_B.']['.self::$LT_L.'](?:
		['.self::$LT_YA.']+['.self::$LT_D.self::$LT_T.']?
		|
		['.self::$LT_I.']+['.self::$LT_D.self::$LT_T.']+
		|
		['.self::$LT_I.']+['.self::$LT_A.']+
	)(?!х)\w* # бля, блядь; не пускает "бляха"
|
	(?:
		\w*['.self::$LT_YI.self::$LT_U.self::$LT_E.self::$LT_A.self::$LT_O.self::$LT_HS.self::$LT_SS.self::$LT_Y.self::$LT_YA.']['.self::$LT_E.self::$LT_YO.self::$LT_YA.self::$LT_I.']['.self::$LT_B.self::$LT_P.'](?!ы\b|ол)\w* # не пускает "еёбы", "наиболее", "наибольшее"...
		|
		['.self::$LT_E.self::$LT_YO.']['.self::$LT_B.']\w*
		|
		['.self::$LT_I.']['. /*self::$LT_P .*/ self::$LT_B.']['.self::$LT_A.']\w+
		|
		['.self::$LT_YI.']['.self::$LT_O.']['.self::$LT_B.self::$LT_P.']\w*
	) # ебать
|
	\w*['.self::$LT_S.']['.self::$LT_C.']?['.self::$LT_U.']+(?:
		['.self::$LT_CH.']*['.self::$LT_K.']+
		|
		['.self::$LT_CH.']+['.self::$LT_K.']*
	)['.self::$LT_A.self::$LT_O.']\w* # сука
|
	\w*(?:
		['.self::$LT_P.']['.self::$LT_I.self::$LT_E.']['.self::$LT_D.']['.self::$LT_A.self::$LT_O.self::$LT_E/* . self::$LT_I*/.']?['.self::$LT_R.'](?!о)\w* # не пускает "Педро"
		|
		['.self::$LT_P.']['.self::$LT_E.']['.self::$LT_D.']['.self::$LT_E.self::$LT_I.']?['.self::$LT_G.self::$LT_K.']
	) # пидарас
|
	\w*['.self::$LT_Z.']['.self::$LT_A.self::$LT_O.']['.self::$LT_L.']['.self::$LT_U.']['.self::$LT_P.']\w* # залупа
|
	\w*['.self::$LT_P.']['.self::$LT_O.']['.self::$LT_T.']['.self::$LT_K.']['.self::$LT_A.']\w* # потка UA
|
	\w*['.self::$LT_D.']['.self::$LT_U.']['.self::$LT_P.']['.self::$LT_A.']\w* # дупа UA
|
	\w*['.self::$LT_H.']['.self::$LT_V.']['.self::$LT_O.']['.self::$LT_YI.']['.self::$LT_D.']['.self::$LT_A.']\w* # хвойда UA
|
	\w*['.self::$LT_M.']['.self::$LT_A.']['.self::$LT_N.']['.self::$LT_D.']['.self::$LT_A.self::$LT_O.']\w* # манда
|
	\w*['.self::$LT_G.']['.self::$LT_A.']['.self::$LT_M.']['.self::$LT_O.']['.self::$LT_N.']\w* # гамон BY
)\b
/xu',
            $text,
            $m
        );

        $c = count($m[1]);

        if ($c > 0) {
            for ($i = 0; $i < $c; $i++) {
                $word = $m[1][$i];
                $word = mb_strtolower($word, $utf8);

                foreach (self::$exceptions as $x) {
                    if (mb_strpos($word, $x) !== false) {
                        $word = false;
                        unset($m[1][$i]);
                        break;
                    }
                }

                if ($word) {
                    $m[1][$i] = str_replace(array(' ', ',', ';', '.', '!', '-', '?', "\t", "\n"), '', $m[1][$i]);
                }
            }

            $m[1] = array_unique($m[1]);

            $asterisks = array();
            foreach ($m[1] as $word) {
                $asterisks [] = str_repeat('*', mb_strlen($word, $utf8));
            }

            $text = str_replace($m[1], $asterisks, $text);
        }

        return $text;
    }
}