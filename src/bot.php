<?php

require __DIR__.'/vendor/autoload.php';

use Dotenv\Dotenv;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Exception\TelegramException;
use Prilichno\Censors\Services\CensorService;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$token = $_ENV['TOKEN'];
$botName = $_ENV['BOTNAME'];

$censorService = new CensorService();
$telegram = new Telegram($token, $botName);
$telegram->useGetUpdatesWithoutDatabase();

echo date('Y-m-d H:i:s').' - Bot started'.PHP_EOL;

while (true) {
    try {
        $response = $telegram->handleGetUpdates();
        if ($response->isOk()) {
            $messages = $response->getResult();
            foreach ($messages as $msg) {
                $message = $msg->message;

                if (isset($message['text'])) {
                    if (!$censorService->isAllowed($message['text'])) {
                        $chatId = $message['chat']['id'];
                        $username = $message['from']['first_name'];

                        $result = Request::sendMessage([
                            'chat_id' => $chatId,
                            'text' => str_replace("#username#", $username, $_ENV['MESSAGE']),
                        ]);
                    }
                }
            }
        } else {
            echo date('Y-m-d H:i:s').' - Failed to fetch updates'.PHP_EOL;
            echo $response->printError();
        }
    } catch (TelegramException $e) {
        echo $e->getMessage();
    }
    sleep(1);
}
